package hdw.api.seckill.domain.dto;

import lombok.Data;

@Data
public class SeckillDTO {

    /**
     * 秒杀商品id
     */
    private Long seckillId;

    /**
     * 存放在缓存中验证码的key
     */
    private String captchaKey;

    /**
     * 用户输入的验证码
     */
    private String captcha;
}
