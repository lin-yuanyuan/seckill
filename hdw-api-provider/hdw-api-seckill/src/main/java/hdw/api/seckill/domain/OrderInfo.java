package hdw.api.seckill.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 订单信息类
 */
@Setter
@Getter
@TableName("t_shop_seckill_order")
public class OrderInfo {
    /**
     * 未付款
     */
    public static final Integer STATUS_ARREARAGE = 0;
    /**
     * 已付款
     */
    public static final Integer STATUS_ACCOUNT_PAID = 1;
    /**
     * 手动取消订单
     */
    public static final Integer STATUS_CANCEL = 2;
    /**
     * 超时取消订单
     */
    public static final Integer STATUS_TIMEOUT = 3;
    /**
     * 已退款
     */
    public static final Integer STATUS_REFUND = 4;
    /**
     * 在线支付
     */
    public static final Integer PAYTYPE_ONLINE = 0;
    /**
     * 积分支付
     */
    public static final Integer PAYTYPE_INTERGRAL = 1;

    /**
     * 订单编号
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long orderNo;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 商品ID(商品服务
     */
    private Long productId;

    /**
     * 收货地址
     */
    private Long deliveryAddrId;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品图片
     */
    private String productImg;

    /**
     * 商品原价
     */
    private BigDecimal productPrice;

    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;

    /**
     * 消耗积分
     */
    private Long intergral;

    /**
     * 订单状态
     */
    private Integer status = STATUS_ARREARAGE;

    /**
     * 订单创建时间
     */
    private String createTime;

    /**
     * 订单支付时间
     */
    private String payTime;

    /**
     * 支付方式 1-在线支付 2-积分支付
     */
    private int payType;

    /**
     * 秒杀的时间
     */
    private String seckillDate;

    /**
     * 秒杀场次
     */
    private Integer seckillTime;

    /**
     * 秒杀商品ID（秒杀服务
     */
    private Long seckillId;
}