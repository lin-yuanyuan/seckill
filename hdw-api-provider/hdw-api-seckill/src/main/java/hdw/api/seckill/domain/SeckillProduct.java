package hdw.api.seckill.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @className: SeckillProduct
 * @Description: <p> 描述：秒杀商品实体类</p>
 * @since: 2024/2/11 18:15
 */
@Data
@TableName("t_shop_seckill_product")
public class SeckillProduct {

    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;

    /**
     * 积分
     */
    private Long intergral;

    /**
     * 库存总数
     */
    private Integer stockCount;

    /**
     * 秒杀日期
     */
    private String startDate;

    /**
     * 秒杀场次
     */
    private Integer time;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

}
