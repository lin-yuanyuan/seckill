package hdw.api.seckill.domain.vo;

import hdw.api.seckill.domain.SeckillProduct;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @className: SeckillProductVO
 * @Description: <p> 描述：秒杀商品信息VO类</p>
 * @since: 2024/2/12 16:38
 */
@Data
public class SeckillProductVO extends SeckillProduct {

    /**
     * 商品图片地址
     */
    private String productImg;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品标题
     */
    private String productTitle;

    /**
     * 商品原价
     */
    private BigDecimal productPrice;

    /**
     * 商品详情
     */
    private String productDetail;

    /**
     * 当前余量
     */
    private Integer currentCount;
}
