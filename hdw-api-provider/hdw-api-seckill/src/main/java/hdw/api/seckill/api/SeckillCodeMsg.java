package hdw.api.seckill.api;

import com.hdw.common.core.api.IErrorCode;

/**
 * @className: SeckillCodeMsg
 * @Description: <p> 描述：秒杀服务API错误代码</p>
 * @since: 2024/2/19 22:54
 */
public enum SeckillCodeMsg implements IErrorCode {

    SECKILL_STOCK_OVER(500201, "您来晚了，商品已经被抢购完毕."),
    REPEAT_SECKILL(500202, "您已经抢购到商品了，请不要重复抢购"),
    SECKILL_ERROR(500203, "秒杀失败"),
    CANCEL_ORDER_ERROR(500204, "超时取消失败"),
    PAY_SERVER_ERROR(500205, "支付服务繁忙，稍后再试"),
    REFUND_ERROR(500206, "退款失败，请联系管理员"),
    INTERGRAL_SERVER_ERROR(500207, "操作积分失败"),
    PRODUCT_SERVER_ERROR(500208, "商品服务繁忙，稍后再试."),
    PAY_ERROR(500209, "支付失败"),
    ILLEGAL_OPERATION(500210, "非法操作");


    private Integer code;
    private String message;

    private SeckillCodeMsg(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
