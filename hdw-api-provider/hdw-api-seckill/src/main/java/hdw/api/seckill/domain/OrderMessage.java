package hdw.api.seckill.domain;

import com.hdw.common.core.vo.LoginUserVo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class OrderMessage implements Serializable {
    /**
     * 秒杀场次
     */
    private Integer time;

    /**
     * 秒杀商品id
     */
    private Long seckillId;

    /**
     * 用户
     */
    private LoginUserVo loginUser;


}
