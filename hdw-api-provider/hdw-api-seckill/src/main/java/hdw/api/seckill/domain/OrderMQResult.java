package hdw.api.seckill.domain;

import lombok.Data;

@Data
public class OrderMQResult {
    /**
     * 秒杀场次
     */
    private Integer time;

    /**
     * 秒杀商品id
     */
    private Long seckillId;

    /**
     * 订单号
     */
    private Long orderNo;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 状态码
     */
    private Integer code;
}
