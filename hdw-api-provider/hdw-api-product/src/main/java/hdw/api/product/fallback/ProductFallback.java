package hdw.api.product.fallback;


import com.hdw.common.core.api.CommonResult;
import hdw.api.product.RemoteProductService;
import hdw.api.product.domain.Product;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @className: ProductFallback
 * @Description: <p> 描述：商品服务降级处理</p>
 * @since: 2024/2/13 23:45
 */
@Component
public class ProductFallback implements RemoteProductService {
    @Override
    public CommonResult<List<Product>> queryByIds(String productIds) {
        //返回兜底数据
        return null;
    }
}
