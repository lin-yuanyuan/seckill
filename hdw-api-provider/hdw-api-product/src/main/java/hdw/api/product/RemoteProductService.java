package hdw.api.product;

import com.hdw.common.core.api.CommonResult;
import hdw.api.product.domain.Product;
import hdw.api.product.fallback.ProductFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @className: RemoteProductService
 * @Description: <p> 描述：商品服务远程调用服务提供者</p>
 * @since: 2024/2/12 18:10
 */
@FeignClient(name = "product-service", fallback = ProductFallback.class)
public interface RemoteProductService {
    /**
     * 通过商品id列表查询对应的商品列表
     *
     * @param productIds
     * @return
     */
    @GetMapping("/product/selectProductListByIds")
    CommonResult<List<Product>> queryByIds(@RequestParam("productIds") String productIds);
}
