package hdw.server.product.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @className: ProductController
 * @Description: <p> 描述：商品服务接口</p>
 * @since: 2024/2/13 23:50
 */
@RestController
@RequestMapping("/product")
public class ProductController {
}
