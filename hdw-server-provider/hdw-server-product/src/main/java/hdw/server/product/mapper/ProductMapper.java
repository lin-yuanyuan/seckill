package hdw.server.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hdw.api.product.domain.Product;
import org.apache.ibatis.annotations.Mapper;

/**
 * @className: ProductMapper
 * @Description: <p> 描述：商品服务mapper接口</p>
 * @since: 2024/2/13 23:55
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {
}
