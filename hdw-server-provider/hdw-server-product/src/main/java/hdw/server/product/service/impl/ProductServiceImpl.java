package hdw.server.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import hdw.api.product.domain.Product;
import hdw.server.product.mapper.ProductMapper;
import hdw.server.product.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @className: ProductServiceImpl
 * @Description: <p> 描述：商品服务service实现类</p>
 * @since: 2024/2/13 23:54
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    @Override
    public List<Product> queryProductList(List<Long> productIds) {
        if (productIds.size() == 0) {
            return Collections.emptyList();
        }
        List<Product> productList = list(new LambdaQueryWrapper<Product>()
                .in(Product::getId, productIds)
                .orderByDesc(Product::getCreateTime));
        return productList;
    }
}
