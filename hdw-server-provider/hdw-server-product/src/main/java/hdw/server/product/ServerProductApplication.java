package hdw.server.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @className: ServerProductApplication
 * @Description: <p> 描述：商品服务启动类</p>
 * @since: 2024/1/28 21:06
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ServerProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerProductApplication.class, args);
    }
}
