package hdw.server.product.rpc;

import com.hdw.common.core.api.CommonResult;
import hdw.api.product.RemoteProductService;
import hdw.api.product.domain.Product;
import hdw.server.product.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @className: RemoteProductServiceImpl
 * @Description: <p> 描述：rpc接口实现类</p>
 * @since: 2024/2/13 23:59
 */
@RestController
@RequestMapping("/product")
public class RemoteProductServiceImpl implements RemoteProductService {
    @Resource
    private ProductService productService;

    @GetMapping("/selectProductListByIds")
    public CommonResult<List<Product>> queryByIds(String productIds) {
        //字符串转换成Long型集合
        String[] productIdArray = productIds.split(",");
        List<Long> idList = Arrays.stream(productIdArray).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());

        return CommonResult.success(productService.queryProductList(idList));
    }
}
