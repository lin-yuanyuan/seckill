package hdw.server.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hdw.api.product.domain.Product;

import java.util.List;

/**
 * @className: ProductService
 * @Description: <p> 描述：商品服务service接口</p>
 * @since: 2024/2/13 23:51
 */
public interface ProductService extends IService<Product> {
    /**
     * 通过id集合查询商品列表
     *
     * @param productIds
     * @return
     */
    List<Product> queryProductList(List<Long> productIds);
}
