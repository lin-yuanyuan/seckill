package hdw.server.seckill.mq;

import com.hdw.api.base.system.service.ISysLogService;
import hdw.api.seckill.domain.OrderMessage;
import hdw.server.seckill.enums.MQConstant;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(consumerGroup = "testGroup", topic = MQConstant.TEST_TOPIC)
public class TestQueueListener implements RocketMQListener<OrderMessage>, RocketMQPushConsumerLifecycleListener {

    private static Logger LOGGER = LoggerFactory.getLogger(OrderPeddingQueueListener.class);

    @DubboReference
    private ISysLogService sysLogService;


    @Override
    public void onMessage(OrderMessage orderMessage) {
        LOGGER.info("*******接收消息********");
        sysLogService.addLog("测试", "拉取消息", 0, 3);
    }

    @Override
    public void prepareStart(DefaultMQPushConsumer defaultMQPushConsumer) {
        //每次拉取的间隔，单位为毫秒
        defaultMQPushConsumer.setPullInterval(1000);
        //设置每次从队列中拉取的消息数为16，则每次拉取的消息理论数值为16 * 1 * 4 = 64条
        defaultMQPushConsumer.setPullBatchSize(16);
    }
}
