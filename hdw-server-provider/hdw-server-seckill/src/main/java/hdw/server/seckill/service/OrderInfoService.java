package hdw.server.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hdw.common.core.vo.LoginUserVo;
import hdw.api.seckill.domain.OrderInfo;
import hdw.api.seckill.domain.dto.SeckillDTO;
import hdw.api.seckill.domain.vo.SeckillProductVO;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;

/**
 * @className: OrderInfoService
 * @Description: <p> 描述：秒杀订单service类</p>
 * @since: 2024/2/19 23:03
 */
public interface OrderInfoService extends IService<OrderInfo> {


    /**
     * 秒杀商品
     *
     * @param time
     * @param seckillId
     * @return
     * @throws ParseException
     */
    String seckillProduct(Integer time, Long seckillId) throws ParseException;

    @Transactional
    OrderInfo doSeckill(SeckillProductVO seckillProductVO, LoginUserVo loginUserVo);

    /**
     * 查看当前登陆用户的订单详情
     *
     * @param orderNo
     * @return
     */
    OrderInfo getDetailByOrderNo(Long orderNo);


    /**
     * 超时取消订单操作
     *
     * @param orderNo
     */
    void cancelOrder(Long orderNo);

    /**
     * 客户端回调查询秒杀结果 返回订单号为秒杀成功 -1L 为秒杀失败 0L 为排队中
     *
     * @param seckillId
     * @return
     */
    Long selectResult(Long seckillId);

    /**
     * 获取秒杀地址
     *
     * @param seckillDTO
     * @return
     */
    String getSeckillPath(SeckillDTO seckillDTO);

}
