package hdw.server.seckill.enums;

public class MQConstant {
    /**
     * 订单队列
     */
    public static final String ORDER_PEDDING_TOPIC = "ORDER_PEDDING_TOPIC";

    /**
     * 订单结果:秒杀成功或失败
     */
    public static final String ORDER_RESULT_TOPIC = "ORDER_RESULT_TOPIC";

    /**
     * 订单超时取消
     */
    public static final String ORDER_PAY_TIMEOUT_TOPIC = "ORDER_PAY_TIMEOUT_TOPIC";

    /**
     * 订单创建成功标识
     */
    public static final String ORDER_RESULT_SUCCESS_TAG = "SUCCESS";

    public static final String TEST_TOPIC = "TEST";
    ;

    /**
     * 订单创建失败标识
     */
    public static final String ORDER_RESULT_FAIL_TAG = "FAIL";

    public static final int ORDER_PAY_TIMEOUT_DELAY_LEVEL = 13;

}
