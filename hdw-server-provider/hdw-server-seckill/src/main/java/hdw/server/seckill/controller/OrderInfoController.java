package hdw.server.seckill.controller;

import com.hdw.common.core.api.CommonResult;
import com.hdw.common.core.exception.BaseException;
import hdw.api.seckill.domain.OrderInfo;
import hdw.api.seckill.domain.dto.SeckillDTO;
import hdw.server.seckill.annotation.AccessLimit;
import hdw.server.seckill.service.OrderInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;

/**
 * @className: OrderInfoController
 * @Description: <p> 描述：秒杀订单接口</p>
 * @since: 2024/2/19 23:01
 */
@RestController
@RequestMapping("/order")
public class OrderInfoController {

    @Resource
    private OrderInfoService orderInfoService;

    /**
     * 动态获取秒杀地址
     *
     * @param seckillDTO
     * @return
     */
    @GetMapping("/path")
    @AccessLimit(second = 5, maxCount = 5, needLogin = true)
    public CommonResult<String> getSeckillPath(SeckillDTO seckillDTO) {
        String seckillPath = orderInfoService.getSeckillPath(seckillDTO);
        return CommonResult.success(seckillPath);
    }


    /**
     * 秒杀功能
     *
     * @param time
     * @param seckillId
     * @return
     */
    @GetMapping("/doSeckill")
    public CommonResult<String> doSeckill(Integer time, Long seckillId) {

        String result;
        try {
            result = orderInfoService.seckillProduct(time, seckillId);
        } catch (BaseException | ParseException e) {
            return CommonResult.failed(e.getMessage());
        }
        return CommonResult.success(result);
    }

    /**
     * 获取订单详情
     *
     * @param orderNo
     * @return
     */
    @GetMapping("/orderInfo")
    public CommonResult<OrderInfo> getOrderInfo(Long orderNo) {
        OrderInfo detail = orderInfoService.getDetailByOrderNo(orderNo);
        return CommonResult.success(detail);
    }


    /**
     * 获取秒杀结果
     *
     * @param seckillId
     * @return
     */
    @GetMapping("/seckillResult")
    public CommonResult<Long> getSeckillResult(Long seckillId) {
        return CommonResult.success(orderInfoService.selectResult(seckillId));
    }

}
