package hdw.server.seckill.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdw.common.core.api.CommonResult;
import com.hdw.common.core.api.ResultCode;
import com.hdw.common.core.vo.LoginUserVo;
import com.hdw.web.base.shiro.ShiroUtil;
import hdw.server.seckill.annotation.AccessLimit;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

/**
 * 接口防刷拦截器（不允许多次请求接口
 */
@Component
public class AccssLimitInterceptor implements HandlerInterceptor {

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            //获取当前用户
            LoginUserVo user = ShiroUtil.getUser();
            HandlerMethod hm = (HandlerMethod) handler;
            AccessLimit accessLimit = hm.getMethodAnnotation(AccessLimit.class);
            if (accessLimit == null) {
                return true;
            }

            int second = accessLimit.second();
            int maxCount = accessLimit.maxCount();
            boolean needLogin = accessLimit.needLogin();

            String key = request.getRequestURI();
            if (needLogin) {
                if (user == null) {
                    render(response, ResultCode.UNAUTHORIZED);
                }
                key += ":" + user.getId();
            }

            Integer count = (Integer) redisTemplate.opsForValue().get(key);
            if (count == null) {
                redisTemplate.opsForValue().set(key, 1, second, TimeUnit.SECONDS);
            } else if (count < maxCount) {
                redisTemplate.opsForValue().increment(key);
            } else {
                render(response, ResultCode.ACCESS_LIMIT_REACHED);
                return false;
            }
        }
        return true;
    }

    /**
     * 创建一个包含错误信息的JSON响应，并将其发送给客户端
     *
     * @param response
     * @param resultCode
     * @throws IOException
     */
    private void render(HttpServletResponse response, ResultCode resultCode) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        //这一行通过response对象获取了一个PrintWriter对象，用于向响应中写入字符数据。
        PrintWriter printWriter = response.getWriter();
        CommonResult bean = CommonResult.failed(resultCode);

        //writeValueAsString方法的作用是将Java对象序列化为JSON字符串。
        printWriter.write(new ObjectMapper().writeValueAsString(bean));

        //这一行将PrintWriter中的所有内容刷新（写出）到响应流中，确保客户端能够接收到数据。
        printWriter.flush();
        printWriter.close();
    }

}
