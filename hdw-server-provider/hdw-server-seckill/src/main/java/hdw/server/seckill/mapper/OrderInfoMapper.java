package hdw.server.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hdw.api.seckill.domain.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @className: OrderInfoMapper
 * @Description: <p> 描述：秒杀订单mapper</p>
 * @since: 2024/2/19 23:02
 */
@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {


}
