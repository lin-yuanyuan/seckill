package hdw.server.seckill.mq;

import hdw.api.seckill.domain.OrderMQResult;
import hdw.server.seckill.enums.MQConstant;
import hdw.server.seckill.service.SeckillProductService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 消费秒杀结果的队列中的失败的消息
 */
@Component
@RocketMQMessageListener(consumerGroup = "OrderResultFailGroup", topic = MQConstant.ORDER_RESULT_TOPIC, selectorExpression = MQConstant.ORDER_RESULT_FAIL_TAG)
public class OrderResultFailQueueListener implements RocketMQListener<OrderMQResult> {

    private static Logger LOGGER = LoggerFactory.getLogger(OrderResultFailQueueListener.class);

    @Resource
    private SeckillProductService seckillProductService;

    @Override
    public void onMessage(OrderMQResult orderMQResult) {
        LOGGER.info("*********************秒杀失败进行库存回补操作***********************");
        //同步数据库的库存到Redis中
        seckillProductService.syncStockToRedis(orderMQResult.getTime(), orderMQResult.getSeckillId());
    }
}
