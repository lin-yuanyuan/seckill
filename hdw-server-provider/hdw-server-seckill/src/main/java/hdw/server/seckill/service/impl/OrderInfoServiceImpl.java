package hdw.server.seckill.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdw.common.core.api.ResultCode;
import com.hdw.common.core.exception.BaseException;

import com.hdw.common.core.utils.DateUtils;
import com.hdw.common.core.vo.LoginUserVo;
import com.hdw.web.base.shiro.ShiroUtil;
import hdw.api.seckill.api.SeckillCodeMsg;
import hdw.api.seckill.domain.OrderInfo;
import hdw.api.seckill.domain.OrderMessage;
import hdw.api.seckill.domain.dto.SeckillDTO;
import hdw.api.seckill.domain.vo.SeckillProductVO;
import hdw.server.seckill.enums.JobRedisKey;
import hdw.server.seckill.enums.MQConstant;
import hdw.server.seckill.mapper.OrderInfoMapper;
import hdw.server.seckill.service.OrderInfoService;
import hdw.server.seckill.service.SeckillProductService;
import hdw.server.seckill.util.DateUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


/**
 * @className: OrderInfoServiceImpl
 * @Description: <p> 描述：秒杀订单service实现类</p>
 * @since: 2024/2/19 23:04
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderInfoServiceImpl.class);
    @Resource
    private SeckillProductService seckillProductService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Resource
    private OrderInfoMapper orderInfoMapper;


    /**
     * 秒杀商品
     *
     * @param time
     * @param seckillId
     * @return
     * @throws BaseException
     * @throws ParseException
     */
    @Override
    public String seckillProduct(Integer time, Long seckillId) throws BaseException, ParseException {
        //通过秒杀商品的id从缓存中获取到秒杀商品的信息
        SeckillProductVO seckillProductVO = seckillProductService.getDetailFromCache(time, seckillId);
        //判断当前时间是否处于秒杀的时间范围内
        boolean legalTime = DateUtil.isLegalTime(seckillProductVO.getStartDate(), time);
//		if(!legalTime){
//			throw new BaseException(SeckillCodeMsg.ILLEGAL_OPERATION);
//		}

        LoginUserVo currentUser = ShiroUtil.getUser();
        if (currentUser == null) {
            throw new BaseException(ResultCode.UNAUTHORIZED);
        }


        //判断是否重复抢购
//        OrderInfo info = CheckIsPurchase(time, seckillId, currentUser);
//        if (info != null) {
//            throw new BaseException(SeckillCodeMsg.REPEAT_SECKILL);
//        }

        /**
         * 优化内容：通过set判断是否重复抢购
         */
        String orderSetKey = JobRedisKey.SECKILL_ORDER_SET.getRealKey(String.valueOf(seckillId));
        if (redisTemplate.opsForSet().isMember(orderSetKey, currentUser.getId())) {
            throw new BaseException(SeckillCodeMsg.REPEAT_SECKILL);
        }


        /**
         * 优化内容：利用redis原子性递减判断库存，保证大多数请求提前被拦截，用于抑制人数从而减少对数据库的访问
         */
        String countKey = JobRedisKey.SECKILL_STOCK_COUNT_HASH.getRealKey(String.valueOf(time));
        Long remainCount = redisTemplate.opsForHash().increment(countKey, String.valueOf(seckillId), -1);
        if (remainCount < 0) {
            throw new BaseException(SeckillCodeMsg.SECKILL_STOCK_OVER);
        }

        //判断库存是否充足
        if (seckillProductVO.getCurrentCount() == 0) {
            throw new BaseException(SeckillCodeMsg.SECKILL_STOCK_OVER);
        }

        /**
         * 优化内容：发送mq消息，使用异步方式进行下单
         */
        OrderMessage message = new OrderMessage(time, seckillId, currentUser);
        rocketMQTemplate.syncSend(MQConstant.ORDER_PEDDING_TOPIC, message);
        return "操作完成，请耐心等候秒杀结果！";

        //进行秒杀
//        OrderInfo orderInfo = doSeckill(seckillProductVO, currentUser);
//        return orderInfo;
    }

    /**
     * 秒杀操作
     *
     * @param seckillProductVO
     * @return
     */
    @Override
    @Transactional
    public OrderInfo doSeckill(SeckillProductVO seckillProductVO, LoginUserVo loginUserVo) {
        //扣减库存
        int result = seckillProductService.updateStorage(seckillProductVO);
        String overSetKey = JobRedisKey.SECKILL_FINISH_ORDER_SET.getPrefix();
        if(result == 0){
            //秒杀结束将id存入set中
            if(!redisTemplate.opsForSet().isMember(overSetKey,seckillProductVO.getId())){
                redisTemplate.opsForSet().add(overSetKey,seckillProductVO.getId());
            }

            throw new BaseException(SeckillCodeMsg.SECKILL_STOCK_OVER);
        }
        //创建订单
        OrderInfo orderInfo = createOrderInfo(seckillProductVO,loginUserVo);

        /***
         *优化项：使用set集合判断是否重复下单---将创建的订单保存到redis的set结构里
         * seckillOrderSet:7 userId
         */
        String orderSetKey = JobRedisKey.SECKILL_ORDER_SET.getRealKey(String.valueOf(seckillProductVO.getId()));
        redisTemplate.opsForSet().add(orderSetKey,loginUserVo.getId());
        return orderInfo;
    }


    /**
     * 创建订单
     *
     * @param seckillProductVO
     * @return
     */
    private OrderInfo createOrderInfo(SeckillProductVO seckillProductVO, LoginUserVo loginUserVo) {
        //创建秒杀订单
        OrderInfo orderInfo = new OrderInfo();
        BeanUtils.copyProperties(seckillProductVO, orderInfo);
        orderInfo.setCreateTime(DateUtils.date2Str(new Date(), DateUtils.DATE_DAY));
        orderInfo.setSeckillDate(seckillProductVO.getStartDate());
        orderInfo.setSeckillTime(seckillProductVO.getTime());
        orderInfo.setSeckillId(seckillProductVO.getId());
        orderInfo.setUserId(loginUserVo.getId());
        LOGGER.info("秒杀订单信息为：{}", orderInfo);
        save(orderInfo);
        return orderInfo;
    }


    /**
     * 判断当前用户是否已经秒杀过当前场次的商品(废弃
     *
     * @param time
     * @param seckillId
     * @param currentUser
     * @return
     */
    private OrderInfo CheckIsPurchase(Integer time, Long seckillId, LoginUserVo currentUser) {
        return getOne(new LambdaQueryWrapper<OrderInfo>()
                .eq(OrderInfo::getSeckillId, seckillId)
                .eq(OrderInfo::getSeckillTime, time)
                .eq(OrderInfo::getUserId, currentUser.getId()));

    }


    /**
     * 用户查看自己的订单详情
     *
     * @param orderNo
     * @return
     */
    @Override
    public OrderInfo getDetailByOrderNo(Long orderNo) {

        if (ShiroUtil.getUser() == null) {
            throw new BaseException(ResultCode.UNAUTHORIZED);
        }
        LoginUserVo loginUser = ShiroUtil.getUser();

        OrderInfo info = getOne(new LambdaQueryWrapper<OrderInfo>()
                .eq(OrderInfo::getOrderNo, orderNo)
                .eq(OrderInfo::getUserId, loginUser.getId()));

        return info;
    }

    @Override
    @Transactional
    public void cancelOrder(Long orderNo) {
        LOGGER.info("超时取消的订单编号为:{}", orderNo);
        OrderInfo order = getOne(new LambdaQueryWrapper<OrderInfo>()
                .eq(OrderInfo::getOrderNo, orderNo));

        //判断订单是否处于未付款状态
        if (OrderInfo.STATUS_ARREARAGE.equals(order.getStatus())) {
            //修改该订单状态为超时取消
            int effectCount = orderInfoMapper.update(null, new LambdaUpdateWrapper<OrderInfo>()
                    .eq(OrderInfo::getOrderNo, orderNo)
                    .eq(OrderInfo::getStatus, 0)
                    .set(OrderInfo::getStatus, OrderInfo.STATUS_TIMEOUT));
            if (effectCount == 0) {
                return;
            }

            //对该订单的秒杀商品进行库存回补
            seckillProductService.incrStockCount(order.getSeckillId());

            //进行Redis的预库存回补
            seckillProductService.syncStockToRedis(order.getSeckillTime(), order.getSeckillId());

        }
        LOGGER.info("*******************执行完毕！***************************");

    }

    /**轮询秒杀结果：-1（秒杀失败） 0（排队中）
     * @param seckillId
     * @return
     */
    @Override
    public Long selectResult(Long seckillId) {
        LoginUserVo currentUser = ShiroUtil.getUser();
        if (currentUser == null) {

        }

        //存放已经秒杀完成的秒杀商品id
        String overSetKey = JobRedisKey.SECKILL_FINISH_ORDER_SET.getPrefix();
        //查询订单
        OrderInfo orderInfo = getOne(new LambdaQueryWrapper<OrderInfo>()
                .eq(OrderInfo::getSeckillId, seckillId)
                .eq(OrderInfo::getUserId, currentUser.getId()));
        if (orderInfo != null) {
            return orderInfo.getOrderNo();
        } else if (redisTemplate.opsForSet().isMember(overSetKey, seckillId)) {
            return -1L;
        } else {
            return 0L;
        }

    }

    public String getSeckillPath(SeckillDTO seckillDTO) {
        LoginUserVo currentUser = ShiroUtil.getUser();
        if (currentUser == null && seckillDTO.getCaptchaKey() == null && seckillDTO.getCaptcha() == null) {
            throw new BaseException(ResultCode.UNAUTHORIZED);
        }
        //判断用户输入的验证码是否正确
        String verCode = (String) redisTemplate.opsForValue().get(seckillDTO.getCaptchaKey());
        if (!seckillDTO.getCaptcha().equals(verCode)) {
            throw new BaseException(ResultCode.ERROR_VERCOD);
        }
        //创建秒杀地址
        return createSeckillPath(currentUser, seckillDTO.getSeckillId());
    }

    /**
     * 创建秒杀地址并存入redis中
     *
     * @param seckillId
     * @return
     */

    public String createSeckillPath(LoginUserVo userVo, Long seckillId) {

        //随机生成一串uuid并加密，根据用户和商品生成路径信息并存入redis 失效时间为1分钟
        String encryptPath = DigestUtils.md5Hex(UUID.randomUUID().toString().replace("-", "") + "123456");
        //seckillPath:1:7
        String key = userVo.getId() + ":" + seckillId;
        redisTemplate.opsForValue().set(JobRedisKey.SECKILL_PATH.getRealKey(key), encryptPath, 1, TimeUnit.MINUTES);

        return encryptPath;
    }


}
