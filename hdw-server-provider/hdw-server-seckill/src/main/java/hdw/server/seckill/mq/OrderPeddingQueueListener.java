package hdw.server.seckill.mq;


import com.hdw.api.base.system.service.ISysLogService;
import hdw.api.seckill.api.SeckillCodeMsg;
import hdw.api.seckill.domain.OrderInfo;
import hdw.api.seckill.domain.OrderMQResult;
import hdw.api.seckill.domain.OrderMessage;
import hdw.api.seckill.domain.vo.SeckillProductVO;
import hdw.server.seckill.enums.MQConstant;
import hdw.server.seckill.service.OrderInfoService;
import hdw.server.seckill.service.SeckillProductService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@RocketMQMessageListener(consumerGroup = "peddingGroup", topic = MQConstant.ORDER_PEDDING_TOPIC)
public class OrderPeddingQueueListener implements RocketMQListener<OrderMessage>, RocketMQPushConsumerLifecycleListener {

    private static Logger LOGGER = LoggerFactory.getLogger(OrderPeddingQueueListener.class);

    @Resource
    private OrderInfoService orderInfoService;

    @Resource
    private SeckillProductService seckillProductService;

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @DubboReference
    private ISysLogService sysLogService;

    @Override
    public void onMessage(OrderMessage orderMessage) {

        LOGGER.info("当前接收到的消息为:{}", orderMessage.toString());
//        sysLogService.addLog("统计","拉取消息",0,2);
        OrderMQResult result = new OrderMQResult();
        String tag;
        try {

            //从缓存中查询秒杀商品信息
            SeckillProductVO vo = seckillProductService.getDetailFromCache(orderMessage.getTime(), orderMessage.getSeckillId());

            //进行下单减库存操作
            OrderInfo info = orderInfoService.doSeckill(vo, orderMessage.getLoginUser());
            result.setOrderNo(info.getOrderNo());
            tag = MQConstant.ORDER_RESULT_SUCCESS_TAG;

            LOGGER.info("秒杀成功，将消息发送到延时队列中。。。。。");
            //秒杀成功----》发送消息到延时队列中,延时时长为10分钟
            Message<OrderMQResult> message = MessageBuilder.withPayload(result).build();
            rocketMQTemplate.syncSend(MQConstant.ORDER_PAY_TIMEOUT_TOPIC, message, 3000, MQConstant.ORDER_PAY_TIMEOUT_DELAY_LEVEL);

        } catch (Exception e) {
            LOGGER.error("异常信息为:{}", e);

            //秒杀失败，发送消息到秒杀失败的队列中处理Redis预库存
            result.setCode(SeckillCodeMsg.SECKILL_ERROR.getCode());
            result.setMsg(SeckillCodeMsg.SECKILL_ERROR.getMessage());
            result.setTime(orderMessage.getTime());
            result.setSeckillId(orderMessage.getSeckillId());
            tag = MQConstant.ORDER_RESULT_FAIL_TAG;

        }

        //发送消息到监听秒杀成功和失败消息的队列中
        rocketMQTemplate.syncSend(MQConstant.ORDER_RESULT_TOPIC + ":" + tag, result);
    }


    @Override
    public void prepareStart(DefaultMQPushConsumer defaultMQPushConsumer) {
        //每次拉取的间隔，单位为毫秒
        defaultMQPushConsumer.setPullInterval(1000);
        //设置每次从队列中拉取的消息数为16，则每次拉取的消息理论数值为16 * 1 * 4 = 64条
        defaultMQPushConsumer.setPullBatchSize(16);
    }
}
