package hdw.server.seckill.controller;

import com.hdw.common.core.api.ResultCode;
import com.hdw.common.core.exception.BaseException;
import com.hdw.common.core.vo.LoginUserVo;
import com.hdw.web.base.shiro.ShiroUtil;
import hdw.api.seckill.domain.OrderMessage;
import hdw.server.seckill.enums.MQConstant;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/v1")
    public void test() {
        OrderMessage orderMessage = new OrderMessage(14, 1L, null);
        rocketMQTemplate.syncSend(MQConstant.TEST_TOPIC, orderMessage);
        System.out.println("*****************发送消息****************");
    }
}
