package hdw.server.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @className: ServerSeckillApplication
 * @Description: <p> 描述：秒杀服务启动类</p>
 * @since: 2024/1/28 21:06
 */
//@SpringBootApplication
//basePackages属性去指明应用程序A在启动的时候需要扫描服务B中的标注了@FeignClient注解的接口的包路径
@EnableFeignClients(basePackages = {"hdw.api.product"})
@EnableDiscoveryClient
@SpringBootApplication
//@SpringBootApplication(exclude = {ShiroAnnotationProcessorAutoConfiguration.class, ShiroAutoConfiguration.class, ShiroBeanAutoConfiguration.class})
@ComponentScan({"hdw.server.seckill.*", "com.hdw.common.*", "com.hdw.web.base.shiro.*"})
@EnableScheduling
public class ServerSeckillApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerSeckillApplication.class, args);
    }
}
