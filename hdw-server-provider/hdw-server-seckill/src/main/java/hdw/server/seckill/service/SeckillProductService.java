package hdw.server.seckill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import hdw.api.seckill.domain.SeckillProduct;
import hdw.api.seckill.domain.vo.SeckillProductVO;

import java.util.List;

/**
 * @className: SeckillProductService
 * @Description: <p> 描述：秒杀商品service类</p>
 * @since: 2024/2/12 15:33
 */

public interface SeckillProductService extends IService<SeckillProduct> {

    /**
     * 通过时间场次查询秒杀列表信息
     *
     * @param time
     * @return
     */
    List<SeckillProductVO> queryByTime(Integer time);

    /**
     * 查看秒杀商品详情
     *
     * @param time
     * @param seckillId
     * @return
     */
    SeckillProductVO getDetail(Integer time, Long seckillId);

    /**
     * 更新秒杀商品库存
     *
     * @param seckillProductVO
     * @return
     */
    int updateStorage(SeckillProductVO seckillProductVO);

    /**
     * 从缓存中获取秒杀商品列表
     *
     * @param time
     * @return
     */
    List<SeckillProductVO> getSeckillProductFromCache(Integer time);

    /**
     * 从缓存中获取秒杀商品详情
     *
     * @param time
     * @param seckillId
     * @return
     */
    SeckillProductVO getDetailFromCache(Integer time, Long seckillId);

    /**
     * 同步数据库的库存到Redis中，进行库存回补
     *
     * @param time
     * @param seckillId
     */
    void syncStockToRedis(Integer time, Long seckillId);

    /**
     * 订单超时对秒杀商品的库存进行回补
     *
     * @param seckillId
     */
    void incrStockCount(Long seckillId);
}
