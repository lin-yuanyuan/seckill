package hdw.server.seckill.mq;

import hdw.api.seckill.domain.OrderMQResult;
import hdw.server.seckill.enums.MQConstant;
import hdw.server.seckill.service.OrderInfoService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@RocketMQMessageListener(consumerGroup = "OrderPayTimeOutGroup", topic = MQConstant.ORDER_PAY_TIMEOUT_TOPIC)
public class OrderPayTimeOutQueueListener implements RocketMQListener<OrderMQResult> {

    private static Logger LOGGER = LoggerFactory.getLogger(OrderPayTimeOutQueueListener.class);

    @Resource
    private OrderInfoService orderInfoService;

    @Override
    public void onMessage(OrderMQResult orderMQResult) {
        LOGGER.info("**************************执行订单超时未支付操作***************************");

        orderInfoService.cancelOrder(orderMQResult.getOrderNo());

    }
}
