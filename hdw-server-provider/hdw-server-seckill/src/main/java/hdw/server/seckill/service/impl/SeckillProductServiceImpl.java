package hdw.server.seckill.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hdw.common.core.api.CommonResult;
import com.hdw.common.core.exception.BaseException;
import com.hdw.common.core.utils.DateUtils;
import hdw.api.product.RemoteProductService;
import hdw.api.product.domain.Product;
import hdw.api.seckill.domain.SeckillProduct;
import hdw.api.seckill.domain.vo.SeckillProductVO;
import hdw.server.seckill.enums.JobRedisKey;
import hdw.server.seckill.mapper.SeckillProductMapper;
import hdw.server.seckill.service.SeckillProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @className: SeckillProductServiceImpl
 * @Description: <p> 描述：秒杀商品业务实现类</p>
 * @since: 2024/2/12 15:32
 */
@Service
public class SeckillProductServiceImpl extends ServiceImpl<SeckillProductMapper, SeckillProduct> implements SeckillProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeckillProductServiceImpl.class);
    @Resource
    private SeckillProductMapper seckillProductMapper;

    @Resource
    private RemoteProductService remoteProductService;

    @Resource
    private RedisTemplate redisTemplate;


    @Override
    public List<SeckillProductVO> queryByTime(Integer time) {

        //1、查询秒杀商品集合数据（场次查询当天的数据）
        List<SeckillProduct> seckillProducts = list(new LambdaQueryWrapper<SeckillProduct>()
                .eq(SeckillProduct::getStartDate, DateUtils.date2Str(new Date(), DateUtils.DATE_DAY))
                .eq(SeckillProduct::getTime, time)
                .orderByDesc(SeckillProduct::getTime));
        if (seckillProducts.size() == 0) {
            return Collections.emptyList();
        }
        LOGGER.info("秒杀商品的数据为:{} ", seckillProducts);

        //2、获取商品id集合
        List<Long> productIdList = seckillProducts.stream().map(SeckillProduct::getProductId).collect(Collectors.toList());
        //Long型集合转换成字符串
        String productIds = productIdList.stream().map(Object::toString).collect(Collectors.joining(","));

        //3、远程获取product表的数据
        CommonResult<List<Product>> result = remoteProductService.queryByIds(productIds);
        if (result == null || result.hasError()) {
            throw new BaseException("重复订购！");
        }

        List<Product> productList = result.getData();
        Map<Long, Product> productMap = productList.stream().collect(Collectors.toMap(Product::getId, Product -> Product));

        //4、封装成VO对象返回
        List<SeckillProductVO> productVoList = seckillProducts.stream().map(seckillProduct -> {
            SeckillProductVO vo = new SeckillProductVO();
            Product product = productMap.get(seckillProduct.getProductId());
            if (product == null) {
                throw new BaseException("该商品id在map中没有对应的商品数据，商品id为: " + seckillProduct.getProductId());
            }
            //拷贝属性，由于VO类的id是秒杀商品的id，因此拷贝属性的时候顺序不能颠倒，先拷贝product的属性再拷贝seckill的属性
            BeanUtils.copyProperties(product, vo);
            BeanUtils.copyProperties(seckillProduct, vo);

            vo.setCurrentCount(seckillProduct.getStockCount());
            return vo;
        }).collect(Collectors.toList());

        return productVoList;
    }

    @Override
    public SeckillProductVO getDetail(Integer time, Long seckillId) {
        //通过seckillId 查询到秒杀商品表的商品信息
        SeckillProduct seckillProduct = getOne(new LambdaQueryWrapper<SeckillProduct>()
                .eq(SeckillProduct::getId, seckillId));

        //查询商品表信息
        CommonResult<List<Product>> result = remoteProductService.queryByIds(String.valueOf(seckillId));
        if (result == null || result.hasError()) {
            throw new BaseException("商品服务出现未知错误！");
        }
        Product product = result.getData().get(0);

        SeckillProductVO seckillProductVO = new SeckillProductVO();
        BeanUtils.copyProperties(product, seckillProductVO);
        BeanUtils.copyProperties(seckillProduct, seckillProductVO);

        seckillProductVO.setCurrentCount(seckillProduct.getStockCount());
        return seckillProductVO;
    }

    @Override
    public int updateStorage(SeckillProductVO seckillProductVO) {

        LOGGER.info("当前秒杀商品的库存余量为:{} ", seckillProductVO.getStockCount());
        return seckillProductMapper.updateStockCount(seckillProductVO.getId());
    }

    @Override
    public List<SeckillProductVO> getSeckillProductFromCache(Integer time) {
        String key = JobRedisKey.SECKILL_PRODUCT_HASH.getRealKey(String.valueOf(time));
        List values = redisTemplate.opsForHash().values(key);
        List<SeckillProductVO> result = (List<SeckillProductVO>) values.stream().map(value -> {
            //转换格式
            SeckillProductVO seckillProductVO = JSON.parseObject((String) value, SeckillProductVO.class);
            LOGGER.info("库存数量为:{}", seckillProductVO.getStockCount());
            return seckillProductVO;
        }).collect(Collectors.toList());
        return result;
    }

    @Override
    public SeckillProductVO getDetailFromCache(Integer time, Long seckillId) {
        String key = JobRedisKey.SECKILL_PRODUCT_HASH.getRealKey(String.valueOf(time));
        Object object = redisTemplate.opsForHash().get(key, String.valueOf(seckillId));
        SeckillProductVO seckillProductVO = JSON.parseObject((String) object, SeckillProductVO.class);
        return seckillProductVO;
    }

    @Override
    public void syncStockToRedis(Integer time, Long seckillId) {
        SeckillProduct product = getOne(new LambdaQueryWrapper<SeckillProduct>()
                .eq(SeckillProduct::getId, seckillId)
                .eq(SeckillProduct::getTime, time));

        if (product.getStockCount() > 0) {
            String key = JobRedisKey.SECKILL_STOCK_COUNT_HASH.getRealKey(String.valueOf(time));
            redisTemplate.opsForHash().put(key, String.valueOf(seckillId), String.valueOf(product.getStockCount()));
        }
    }

    @Override
    public void incrStockCount(Long seckillId) {
        seckillProductMapper.incrStockCount(seckillId);
    }


    @Scheduled(cron = "1 * * * * ? ")
//@Scheduled(cron = "59 59 23 * * ? ")
    void timingUpdateSeckillProduct() {
        LOGGER.info("********************************************定时上架的任务开启*****************************");
        int[] timeArray = {12, 14, 16};
        List<SeckillProductVO> seckillProductList;
        //获取秒杀商品列表
        for (int i = 0; i < timeArray.length; i++) {
            LOGGER.info("当前保存到Redis的秒杀商品的场次为：{}", timeArray[i]);
            seckillProductList = this.queryByTime(timeArray[i]);

            //删除之前的数据
            String key = JobRedisKey.SECKILL_PRODUCT_HASH.getRealKey(String.valueOf(timeArray[i]));
            String countKey = JobRedisKey.SECKILL_STOCK_COUNT_HASH.getRealKey(String.valueOf(timeArray[i]));

            redisTemplate.delete(key);
            redisTemplate.delete(countKey);

            //存储集合的数据到redis中
            for (SeckillProductVO seckillProduct : seckillProductList) {
                //存储秒杀商品数据到Redis中
                redisTemplate.opsForHash().put(key, String.valueOf(seckillProduct.getId()), JSON.toJSONString(seckillProduct));
                //存储秒杀商品的库存数据到Redis中
                redisTemplate.opsForHash().put(countKey, String.valueOf(seckillProduct.getId()), seckillProduct.getStockCount());
            }


        }
        LOGGER.info("****************************************更新完毕，明天同一时间不见不散！****************************");

    }


}
