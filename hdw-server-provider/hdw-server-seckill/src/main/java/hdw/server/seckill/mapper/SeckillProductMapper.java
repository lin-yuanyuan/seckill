package hdw.server.seckill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import hdw.api.seckill.domain.SeckillProduct;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @className: SeckillProductMapper
 * @Description: <p> 描述：秒杀商品mapper接口</p>
 * @since: 2024/2/12 15:28
 */
@Mapper
public interface SeckillProductMapper extends BaseMapper<SeckillProduct> {

    @Update("update t_shop_seckill_product set stock_count = stock_count +1 where id = #{seckillId}")
    int incrStockCount(@Param("seckillId") Long seckillId);

    @Update("update t_shop_seckill_product set stock_count = stock_count -1 where id = #{seckillId} and stock_count >0")
    int updateStockCount(@Param("seckillId") Long seckillId);

}
