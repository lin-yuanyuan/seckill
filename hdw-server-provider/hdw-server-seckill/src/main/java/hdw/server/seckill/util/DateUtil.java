package hdw.server.seckill.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @className: DateUtil
 * @Description: <p> 描述：时间工具类</p>
 * @since: 2024/2/19 23:50
 */
public class DateUtil {

    /**
     * 传入秒杀时间和场次判断当前操作是否处于秒杀时间段内
     *
     * @param date
     * @param time
     * @return
     * @throws ParseException
     */
    public static boolean isLegalTime(String date, int time) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(simpleDateFormat.parse(date));
        //用于设置日历字段的值
        calendar.set(Calendar.HOUR_OF_DAY, time);
        Long seckillStart = calendar.getTime().getTime();
        //获取当前毫秒数
        Long now = System.currentTimeMillis();
        calendar.add(Calendar.HOUR_OF_DAY, 2);

        Long seckillEnd = calendar.getTime().getTime();
        return now >= seckillStart && now <= seckillEnd;

    }
}
