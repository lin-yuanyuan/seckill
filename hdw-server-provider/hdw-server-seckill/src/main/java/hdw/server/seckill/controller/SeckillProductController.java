package hdw.server.seckill.controller;

import com.hdw.common.core.api.CommonResult;
import hdw.api.seckill.domain.vo.SeckillProductVO;
import hdw.server.seckill.service.SeckillProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @className: SeckillProductController
 * @Description: <p> 描述：秒杀商品相关接口</p>
 * @since: 2024/2/12 15:27
 */
@RestController
@RequestMapping("/seckillProduct")
public class SeckillProductController {

    @Resource
    private SeckillProductService seckillProductService;

    /**
     * 查询某个场次的商品列表
     *
     * @param time
     * @return
     */
    @GetMapping("/queryByTime")
    public CommonResult<List<SeckillProductVO>> queryByTime(Integer time) {
        List<SeckillProductVO> seckillProductVOS = seckillProductService.getSeckillProductFromCache(time);
        return CommonResult.success(seckillProductVOS);
    }

    /**
     * 查看秒杀商品详情
     *
     * @param time
     * @param seckillId
     * @return
     */
    @GetMapping("/seckillProductDetail")
    public CommonResult<SeckillProductVO> getSeckillProductDetail(Integer time, Long seckillId) {
        SeckillProductVO seckillProductVO = seckillProductService.getDetailFromCache(time, seckillId);
        return CommonResult.success(seckillProductVO);
    }
}
