package com.hdw.common.core.utils;

import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 时间戳工具类
 *
 * @author Administrator
 */
public class DateUtils {

    public static final String DATE_MIN = "yyyy-MM-dd HH:mm";

    public static final String DATE_HOUR = "yyyy-MM-dd HH";

    public static final String DATE_HOUR_CH = "yyyy年MM月dd日HH时";

    public static final String DATE_DAY = "yyyy-MM-dd";

    public static final String DATE_DAY_CH = "yyyy年MM月dd日";

    public static final String DATE_SEC = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_SEC_S = "yyyy-MM-dd HH:mm:ss.S";

    public static final String DATE_MD0000_Z = "yyyy-01-01'T'00:00:00.000'Z'";
    public static final String DATE_MD1231_Z = "yyyy-12-31'T'23:59:59.999'Z'";

    public static final String DATE_SEC_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_SEC_Z00 = "yyyy-MM-dd'T'00:00:00.000'Z'";
    public static final String DATE_SEC_Z23 = "yyyy-MM-dd'T'23:59:59.999'Z'";

    public static final String YYYYMMDD = "yyyyMMdd";

    public static final String YYYY_MM_DD_HH_MM = "yyyyMMddHHmm";

    public static final String YYYY_MM_DD_HH = "yyyyMMddHH";

    public static final String YYYY_MM_DD_HH_MM_SS = "yyyyMMddHHmmss";

    public static final String YYYY_MM_DD_HH_MM_SS_SSS = "yyyyMMddHHmmssSSS";

    private static SimpleDateFormat sdf19_sec = new SimpleDateFormat(DATE_SEC);

    private static SimpleDateFormat sdf16_min = new SimpleDateFormat(DATE_MIN);

    private static SimpleDateFormat sdf13_hour = new SimpleDateFormat(DATE_HOUR);

    private static SimpleDateFormat sdf10_day = new SimpleDateFormat(DATE_DAY);

    private static SimpleDateFormat sdf10_hour = new SimpleDateFormat(YYYY_MM_DD_HH);

    private static SimpleDateFormat sdf23_ms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private static SimpleDateFormat sdf14_sec = new SimpleDateFormat("yyyyMMddHHmmss");

    private static SimpleDateFormat sdf14_sec2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static SimpleDateFormat sdf17_ms = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    private static SimpleDateFormat sdf8_day = new SimpleDateFormat("yyyyMMdd");

    private static SimpleDateFormat sdf6_day = new SimpleDateFormat("yyyyMM");

    private static final SimpleDateFormat sdf5 = new SimpleDateFormat(
            "yyyyMMddHH00");
    private static SimpleDateFormat sdf20_sec = new SimpleDateFormat(DATE_SEC_Z00);

    /**
     * 格式化日期输出
     *
     * @param d
     * @return
     */
    public static Date formatDate(String d) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date str142Date(String date) throws ParseException {
        synchronized (sdf14_sec2) {
            return sdf14_sec2.parse(date);
        }
    }


    public static Date str19ToDate2(String date) throws ParseException {
        synchronized (sdf19_sec) {
            return sdf19_sec.parse(date);
        }
    }


    /**
     * 指定时间 指定加减分钟 指定格式输出
     *
     * @param timeStr
     * @param n
     * @param pattern
     * @return
     */
    public static String getPatternMinStr(String timeStr, String tpattern, int n, String pattern) {
        String reStr = null;
        try {
            // "yyyyMMddHH00"
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            SimpleDateFormat sdf2 = new SimpleDateFormat(tpattern);
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTime(sdf2.parse(timeStr));
            //rightNow.add(Calendar.YEAR, -1);// 日期减1年
            //rightNow.add(Calendar.MONTH, 3);// 日期加3个月
            //rightNow.add(Calendar.DAY_OF_YEAR, 10);// 日期加10天
            rightNow.add(Calendar.MINUTE, n);// 加减n分钟
            reStr = sdf.format(rightNow.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reStr;
    }

    /**
     * @param n       正数加n小时 负数减n小时
     * @param pattern 指定格式  yyyyMMddHH00
     * @return yyyyMMddHH00
     */
    public static String getHourStr(int n, String pattern) {
        String reStr = null;
        try {
            // "yyyyMMddHH00"
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTime(new Date());
            //rightNow.add(Calendar.YEAR, -1);// 日期减1年
            //rightNow.add(Calendar.MONTH, 3);// 日期加3个月
            //rightNow.add(Calendar.DAY_OF_YEAR, 10);// 日期加10天
            rightNow.add(Calendar.HOUR_OF_DAY, n);// 加减n小时
            reStr = sdf.format(rightNow.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reStr;
    }


    /**
     * 字符串转换成日期 如果转换格式为空，则利用默认格式进行转换操作
     *
     * @param str    字符串
     * @param format 日期格式
     * @return 日期
     * @throws ParseException
     */
    public static Date str2Date(String str, String format) {
        if (null == str || "".equals(str)) {
            return null;
        }
        if (null == format || "".equals(format)) {
            format = DATE_SEC;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(str);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 日期转换为字符串
     *
     * @param date   日期
     * @param format 日期格式
     * @return 字符串
     */
    public static String date2Str(Date date, String format) {
        if (null == date) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * 字符串转换时间戳
     *
     * @param str
     * @return
     */
    public static Timestamp str2Timestamp(String str) {
        Date date = str2Date(str, DATE_SEC);
        return new Timestamp(date.getTime());
    }

    /**
     * 时间加上多少小时
     *
     * @param date
     * @param hour
     * @return
     */
    public static Date timeAddHour(Date date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hour);
        return cal.getTime();
    }


    /**
     * 日期字符串加上小时
     *
     * @param time    日期
     * @param format1 原先日期格式
     * @param format2 要转换的日期格式，不转则传null
     * @param hour    小时数
     * @return
     */
    public static String timeAddHour(String time, String format1, String format2, int hour) {
        try {
            Date date = new SimpleDateFormat(format1).parse(time);
            Date addHour = timeAddHour(date, hour);
            if (StringUtils.isNotEmpty(format2)) {
                return format(addHour, format2);
            } else {
                return format(addHour, format1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 指定加减n小时
     *
     * @param dateStr 北京时
     * @param n       正数为加,负数为减
     * @return 世界时str
     */
    public static String getPatternDateStr(String dateStr, String spattern, String epattern, int n) {
        if (dateStr == null || dateStr.equals("")) {
            return dateStr;
        }
        String reStr = null;
        try {
            // "yyyyMMddHH00"
            SimpleDateFormat sdf = new SimpleDateFormat(spattern);
            SimpleDateFormat sdf2 = new SimpleDateFormat(epattern);
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTime(sdf.parse(dateStr));
            // rightNow.add(Calendar.YEAR, -1);// 日期减1年
            // rightNow.add(Calendar.MONTH, 3);// 日期加3个月
            // rightNow.add(Calendar.DAY_OF_YEAR, 10);// 日期加10天
            rightNow.add(Calendar.HOUR_OF_DAY, n);// 加减n小时
            reStr = sdf2.format(rightNow.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return reStr;
    }

    /**
     * 获取当前日期是星期几<br>
     *
     * @param dt
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);

        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    private final static String GMT = "GMT";

    /**
     * 传入 任意两个yyyyMMddHH
     * 得到他们之间所有的 yyyyMMdd的list集合
     *
     * @param startTime yyyyMMddHH
     * @param endTime   yyyyMMddHH
     * @return List<String>
     */
    public static List<String> convertDateList(String startTime, String endTime) {

        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD_HH);
        SimpleDateFormat sdf2 = new SimpleDateFormat(YYYYMMDD);
        Date date = null;
        List<String> dateList = new ArrayList<String>();
        String pattern = YYYYMMDD;
        try {
            date = sdf.parse(startTime);
            int d = DateUtils.daysBetween(convertDatePattern(startTime, pattern), convertDatePattern(endTime, pattern), pattern);
            for (int i = 0; i <= d; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.HOUR_OF_DAY, i * 24);// 加减n小时
                dateList.add(sdf2.format(calendar.getTime()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateList;
    }

    public static Date parse(String strDate, String pattern) throws ParseException {
        return null == strDate || strDate.equals("") ? null : new SimpleDateFormat(pattern).parse(strDate);
    }

    public static String formatDate(String dateStr, String informat, String outformat) {
        Date date = new Date();
        try {
            date = parse(dateStr, informat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return format(date, outformat);
    }

    /**
     * 将yyyyMMddHHmmss
     * 转换成指定格式
     *
     * @param dateStr
     * @return
     */
    public static String convertDatePattern(String dateStr, String pattern) {
        String convertStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD_HH);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern);
        try {
            Date date = sdf.parse(dateStr);
            convertStr = sdf2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertStr;
    }

    /**
     * 传入 任意两个时间
     * 得到他们之间所有的时间的list集合
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param format    时间格式
     * @return List<String>
     */
    public static List<String> convertDateList(String startTime, String endTime, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        SimpleDateFormat sdf2 = new SimpleDateFormat(format);
        Date date = null;
        List<String> dateList = new ArrayList<>();
        try {
            date = sdf.parse(startTime);
            int d = DateUtils.daysBetween(startTime, endTime, format);
            for (int i = 0; i <= d; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.HOUR_OF_DAY, i * 24);// 加减n小时
                dateList.add(sdf2.format(calendar.getTime()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateList;
    }

    /**
     * 传入两个时间生成两个时间之间的时间数组，并放入list返回
     *
     * @param date1        开始时间
     * @param date2        结束时间
     * @param dateFormat   两个时间参数的格式，例如：yyyy-MM-dd、yyyyMMdd、yyyyMMddHHmmss等
     * @param timeInterval 时间间隔，单位是分钟，例如1小时则传入60
     */
    public static List<String> getAllTimeList(String date1, String date2, String dateFormat, int timeInterval) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        List<String> dateList = new ArrayList<String>();

        String tmp;
        try {
            int betwenDay = daysBetween(date1, date2, dateFormat);
            if (betwenDay < 0 || timeInterval < 1) {
                return dateList;
            }//开始时间小于结束时间、或者间隔太小，退出
            dateList.add(date1);
            if (date1.equals(date2)) {
                return dateList;
            }

            int interval = 1000 * 60 * timeInterval;//时间间隔
            tmp = format.format(format.parse(date1).getTime() + interval);
            while (tmp.compareTo(date2) <= 0) {
                dateList.add(tmp);
                tmp = format.format(format.parse(tmp).getTime() + interval);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateList;
    }

    /**
     * 指定格式时间字符串 计算两个字符串时间相差几天
     *
     * @param startTime
     * @param endTime
     * @return
     * @throws ParseException
     */
    public static int daysBetween(String startTime, String endTime, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(startTime));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(endTime));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * 获取两个时间之间相差的天数
     *
     * @param firsttime1
     * @param secondtime2
     * @param format      日期格式
     * @return
     */
    public static int getBetweenDayByTimeStr(String firsttime1, String secondtime2, String format) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(firsttime1));
            long time1 = cal.getTimeInMillis();
            cal.setTime(sdf.parse(secondtime2));
            long time2 = cal.getTimeInMillis();
            long between_days = (time2 - time1) / (1000 * 3600 * 24);
            if (time1 > time2) {
                return -1;
            }
            return Integer.parseInt(String.valueOf(between_days));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return -1;
    }


    public static String format(Date date, String pattern) {
        return date == null ? "" : new SimpleDateFormat(pattern).format(date);
    }

    /**
     * 获取某天的开始时间
     *
     * @param date
     * @return Date
     * @author shanghong wan
     * @date 2021/9/16 16:13
     * @since v0.4
     */
    public static Date getTimeStart(Date date) {
        if (date == null)
            return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取某天的结束时间
     *
     * @param date
     * @return Date
     * @author shanghong wan
     * @date 2021/9/16 16:13
     * @since v0.4
     */
    public static Date getTimeEnd(Date date) {
        if (date == null)
            return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * 获取传入时间的下一个整点时间
     *
     * @param date
     * @return
     */
    public static Date getNextWholeHourDate(Date date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        ca.add(Calendar.HOUR, 1);
        ca.set(Calendar.MINUTE, 0);
        ca.set(Calendar.SECOND, 0);
        ca.set(Calendar.MILLISECOND, 0);
        return ca.getTime();
    }

    /**
     * 获取传入时间的整点时间
     *
     * @param date
     * @return
     */
    public static Date getCurrentWholeHourDate(Date date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        ca.set(Calendar.MINUTE, 0);
        ca.set(Calendar.SECOND, 0);
        ca.set(Calendar.MILLISECOND, 0);
        return ca.getTime();
    }

    /**
     * 在原日期的基础上增加天数
     *
     * @param date
     * @param i
     * @return
     */
    public static Date addDay(Date date, int i) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, i);
        Date newDate = c.getTime();
        return newDate;
    }

    /**
     * 某种格式转换成某种格式
     *
     * @param sourceDateStr 源时间字符串
     * @param sourcePattern 源时间格式
     * @param targetPattern 目标时间格式字符串
     * @return
     * @throws ParseException
     * @author shanghong wan
     */
    public static String strToStr(String sourceDateStr, String sourcePattern, String targetPattern) throws ParseException {
        SimpleDateFormat sourceSimpleDateFormat = new SimpleDateFormat(sourcePattern);
        synchronized (sourceSimpleDateFormat) {
            Date sourceDate = sourceSimpleDateFormat.parse(sourceDateStr);
            SimpleDateFormat targetSimpleDateFormat = new SimpleDateFormat(targetPattern);
            return targetSimpleDateFormat.format(sourceDate);
        }
    }


    /**
     * 在原日期的基础上增减数
     *
     * @param date 原本时间
     * @param type 增减类型
     * @param num  增减数
     */
    public static Date addTime(Date date, int type, int num) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(type, num);
        return c.getTime();
    }

    /**
     * 获取日历上某个月的开始日期（按星期天为第一天）
     *
     * @param year
     * @param month
     * @return
     */
    public static String getStartCalendar(Integer year, Integer month) {
        Calendar start = Calendar.getInstance();
        start.set(Calendar.MONTH, month - 1);  // month is 0 based on calendar
        start.set(Calendar.YEAR, year);
        start.set(Calendar.DAY_OF_MONTH, 1);
        start.getTime();
        start.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

        return sdf20_sec.format(start.getTime());
    }

    /**
     * 获取日历上某个月的结束日期（按星期天为第一天）
     *
     * @param year
     * @param month
     * @return
     */
    public static String getEndCalendar(Integer year, Integer month) {
        Calendar end = Calendar.getInstance();
        end.set(Calendar.MONTH, month);  // next month
        end.set(Calendar.YEAR, year);
        end.set(Calendar.DAY_OF_MONTH, 1);
        end.getTime();
        end.set(Calendar.DATE, -1);
        end.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        //方便查询， 比日历多加一天
        end.add(Calendar.DATE, +1);
        if (end.get(Calendar.MONTH) != month)
            end.add(Calendar.DATE, +7);
        return sdf20_sec.format(end.getTime());
    }

    /**
     * 获取传入时间月最后一天
     */
    public static String getLastDayOfMonth(String time, String format) {
        Date date = str2Date(time, format);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.roll(Calendar.DAY_OF_MONTH, -1);
        return format(calendar.getTime(), format);
    }

    public static boolean checkFormat(String time, String format) {
        DateFormat sdf = new SimpleDateFormat(format);
        sdf.setLenient(false);
        try {
            sdf.parse(time);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

}
