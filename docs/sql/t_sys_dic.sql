create table t_sys_dic
(
    id          bigint auto_increment comment '主键ID'
        primary key,
    parent_id   bigint       not null comment '父变量ID',
    var_code    varchar(255) null comment '变量代码',
    var_name    varchar(255) null comment '变量名称',
    create_time datetime     null comment '记录创建时间',
    update_time datetime     null comment '记录修改时间',
    create_user varchar(255) null comment '记录创建者（用户）',
    update_user varchar(255) null comment '记录最后修改者（用户）',
    is_sync     tinyint      null comment '数据是否同步(0:是,1:否)'
)
    comment '数据字典表' charset = utf8
                         row_format = DYNAMIC;

create index index_dic_code
    on t_sys_dic (var_code);

create index index_dic_name
    on t_sys_dic (var_name);

