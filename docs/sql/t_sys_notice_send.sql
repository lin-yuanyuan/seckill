create table t_sys_notice_send
(
    ID          bigint auto_increment
        primary key,
    NOTICE_ID   bigint      null comment '通告ID',
    USER_ID     bigint      null comment '用户id',
    READ_FLAG   varchar(10) null comment '阅读状态（0未读，1已读）',
    READ_TIME   datetime    null comment '阅读时间',
    CREATE_USER varchar(32) null comment '创建人',
    CREATE_TIME datetime    null comment '创建时间',
    UPDATE_USER varchar(32) null comment '更新人',
    UPDATE_TIME datetime    null comment '更新时间'
)
    comment '用户通告阅读标记表' charset = utf8
                                 row_format = DYNAMIC;

INSERT INTO hdw_dubbo.t_sys_notice_send (ID, NOTICE_ID, USER_ID, READ_FLAG, READ_TIME, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME) VALUES (12, 4, 1, '0', null, 'admin', '2020-11-22 17:48:14', 'admin', '2020-11-22 17:48:14');
INSERT INTO hdw_dubbo.t_sys_notice_send (ID, NOTICE_ID, USER_ID, READ_FLAG, READ_TIME, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME) VALUES (13, 5, 1, '0', null, 'admin', '2020-11-22 17:48:14', 'admin', '2020-11-22 17:48:14');
INSERT INTO hdw_dubbo.t_sys_notice_send (ID, NOTICE_ID, USER_ID, READ_FLAG, READ_TIME, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME) VALUES (14, 6, 1, '0', null, 'admin', '2020-11-22 17:48:14', 'admin', '2020-11-22 17:48:14');
INSERT INTO hdw_dubbo.t_sys_notice_send (ID, NOTICE_ID, USER_ID, READ_FLAG, READ_TIME, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME) VALUES (15, 7, 1, '0', null, 'admin', '2020-11-22 17:48:14', 'admin', '2020-11-22 17:48:14');
