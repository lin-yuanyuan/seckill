create table t_sys_user_enterprise
(
    id            bigint auto_increment comment '主键id'
        primary key,
    user_id       bigint null comment '角色id',
    enterprise_id bigint null comment '企业id'
)
    comment '监管用户与企业关联表' charset = utf8
                                   row_format = DYNAMIC;

create index user_id
    on t_sys_user_enterprise (user_id, enterprise_id);

