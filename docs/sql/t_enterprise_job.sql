create table t_enterprise_job
(
    id            bigint auto_increment
        primary key,
    department_id bigint       null comment '企业部门表ID',
    job_code      varchar(50)  null comment '职务代码',
    job_name      varchar(50)  null comment '职务名称',
    create_time   datetime     null on update CURRENT_TIMESTAMP comment '记录创建时间',
    update_time   datetime     null on update CURRENT_TIMESTAMP comment '记录最后更新时间',
    create_user   varchar(255) null comment '记录创建用户',
    update_user   varchar(255) null comment '记录最后更新用户',
    parameter1    varchar(255) null comment '预留1',
    parameter2    varchar(255) null comment '预留2'
)
    comment '企业职务配置表' charset = utf8
                             row_format = DYNAMIC;

