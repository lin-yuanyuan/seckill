create table t_enterprise_department
(
    id              bigint auto_increment
        primary key,
    parent_id       bigint       null comment '企业部门父ID',
    enterprise_id   bigint       null comment '企业ID(对应企业主表ID)',
    department_code varchar(255) null comment '部门代码(可添加多个部门ID，用逗号隔开，表示该部门可以管理多个部门)',
    department_name varchar(100) null comment '部门名称',
    create_time     datetime     null comment '记录创建时间',
    update_time     datetime     null comment '记录最后修改时间',
    create_user     varchar(20)  null comment '记录创建者(用户)',
    update_user     varchar(20)  null comment '记录最后修改者(用户)',
    parameter1      varchar(255) null comment '预留1',
    parameter2      varchar(255) null comment '预留2'
)
    comment '企业部门表' charset = utf8
                         row_format = DYNAMIC;

