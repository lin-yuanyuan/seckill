create table qrtz_locks
(
    SCHED_NAME varchar(120) not null,
    LOCK_NAME  varchar(40)  not null,
    primary key (SCHED_NAME, LOCK_NAME)
)
    charset = utf8
    row_format = DYNAMIC;

INSERT INTO hdw_dubbo.qrtz_locks (SCHED_NAME, LOCK_NAME) VALUES ('hdw-server-base-Scheduler', 'STATE_ACCESS');
INSERT INTO hdw_dubbo.qrtz_locks (SCHED_NAME, LOCK_NAME) VALUES ('hdw-server-base-Scheduler', 'TRIGGER_ACCESS');
