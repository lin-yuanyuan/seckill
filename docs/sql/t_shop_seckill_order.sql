create table t_shop_seckill_order
(
    order_no         bigint         not null comment '秒杀订单id，主键'
        primary key,
    user_id          bigint         null comment '用户id',
    product_id       bigint         null comment '商品id',
    product_img      varchar(255)   null comment '商品图片',
    delivery_addr_id bigint         null comment '收货地址',
    product_name     varchar(32)    null comment '商品名称',
    product_price    decimal(10, 2) null comment '商品原价',
    seckill_price    decimal(10, 2) null comment '秒杀价格',
    status           tinyint        null comment '订单状态',
    create_time      varchar(32)    null comment '订单创建时间',
    pay_time         varchar(32)    null comment '订单支付时间',
    seckill_time     varchar(32)    null comment '秒杀场次',
    seckill_date     varchar(32)    null comment '秒杀时间',
    intergral        decimal        null comment '消耗积分',
    seckill_id       bigint         null comment '秒杀商品id',
    pay_type         tinyint        null comment '支付方式',
    constraint user_id
        unique (user_id, seckill_id)
)
    comment '秒杀订单表';

INSERT INTO hdw_dubbo.t_shop_seckill_order (order_no, user_id, product_id, product_img, delivery_addr_id, product_name, product_price, seckill_price, status, create_time, pay_time, seckill_time, seckill_date, intergral, seckill_id, pay_type) VALUES (1788106050637950978, 1, 12, 'https://img12.360buyimg.com/n1/s450x450_jfs/t1/181402/6/37654/52230/64eee783Fa968b60e/2b4023d00fec4f48.jpg.avif', null, '魅族有线耳机', 119.00, 100.00, 3, '2024-05-08', null, '14', '2024-05-08', null, 26, 0);
INSERT INTO hdw_dubbo.t_shop_seckill_order (order_no, user_id, product_id, product_img, delivery_addr_id, product_name, product_price, seckill_price, status, create_time, pay_time, seckill_time, seckill_date, intergral, seckill_id, pay_type) VALUES (1788117143527628801, 1, 1, 'https://img13.360buyimg.com/n1/s450x450_jfs/t1/192110/34/31853/13723/6395ab62Ed3266c2b/e7d7f61ca8d1d5ca.jpg.avif', null, '小米13', 2999.00, 2000.00, 3, '2024-05-08', null, '16', '2024-05-08', null, 29, 0);
INSERT INTO hdw_dubbo.t_shop_seckill_order (order_no, user_id, product_id, product_img, delivery_addr_id, product_name, product_price, seckill_price, status, create_time, pay_time, seckill_time, seckill_date, intergral, seckill_id, pay_type) VALUES (1788132914366521346, 1, 2, 'https://img12.360buyimg.com/n1/s450x450_jfs/t1/206736/7/30752/28217/65d72e58F1da31ca6/6607e3391affa86e.jpg.avif', null, '华为Pocket 2', 8999.00, 8000.00, 0, '2024-05-08', null, '16', '2024-05-08', null, 30, 0);
