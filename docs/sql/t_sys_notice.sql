create table t_sys_notice
(
    ID           bigint auto_increment
        primary key,
    TITLE        varchar(100)            null comment '标题',
    MSG_ABSTRACT text                    null comment '摘要',
    MSG_CONTENT  text                    null comment '内容',
    START_TIME   datetime                null comment '开始时间',
    END_TIME     datetime                null comment '结束时间',
    PRIORITY     varchar(255)            null comment '优先级（L低，M中，H高）',
    MSG_CATEGORY varchar(10) default '2' not null comment '消息类型1:通知公告2:系统消息',
    MSG_TYPE     varchar(10)             null comment '通告对象类型（USER:指定用户，ALL:全体用户）',
    SEND_STATUS  varchar(10)             null comment '发布状态（0未发布，1已发布，2已撤销）',
    SEND_TIME    datetime                null comment '发布时间',
    CANCEL_TIME  datetime                null comment '撤销时间',
    USER_IDS     text                    null comment '指定用户',
    BUS_TYPE     varchar(20)             null comment '业务类型(email:邮件 bpm:流程)',
    BUS_ID       varchar(50)             null comment '业务id',
    OPEN_TYPE    varchar(20)             null comment '打开方式(组件：component 路由：url)',
    OPEN_PAGE    varchar(255)            null comment '组件/路由 地址',
    CREATE_USER  varchar(32)             null comment '创建人',
    CREATE_TIME  datetime                null comment '创建时间',
    UPDATE_USER  varchar(32)             null comment '更新人',
    UPDATE_TIME  datetime                null comment '更新时间',
    DEL_FLAG     varchar(1)              null comment '删除状态（0，正常，1已删除）'
)
    comment '系统通告表' charset = utf8
                         row_format = DYNAMIC;

INSERT INTO hdw_dubbo.t_sys_notice (ID, TITLE, MSG_ABSTRACT, MSG_CONTENT, START_TIME, END_TIME, PRIORITY, MSG_CATEGORY, MSG_TYPE, SEND_STATUS, SEND_TIME, CANCEL_TIME, USER_IDS, BUS_TYPE, BUS_ID, OPEN_TYPE, OPEN_PAGE, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME, DEL_FLAG) VALUES (4, '测试1', '测试1', '测试', '2020-10-14 15:20:00', '2020-12-31 23:59:59', 'H', '2', 'ALL', '1', '2020-11-22 17:45:22', null, null, null, null, null, null, 'admin', '2020-09-21 15:19:22', 'admin', '2020-11-22 17:45:22', '0');
INSERT INTO hdw_dubbo.t_sys_notice (ID, TITLE, MSG_ABSTRACT, MSG_CONTENT, START_TIME, END_TIME, PRIORITY, MSG_CATEGORY, MSG_TYPE, SEND_STATUS, SEND_TIME, CANCEL_TIME, USER_IDS, BUS_TYPE, BUS_ID, OPEN_TYPE, OPEN_PAGE, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME, DEL_FLAG) VALUES (5, '测试2', '测试2', '测试2', '2020-10-14 11:20:00', '2020-12-31 23:59:59', 'H', '1', 'ALL', '1', '2020-11-22 17:45:14', null, null, null, null, null, null, 'admin', '2020-09-21 16:58:50', 'admin', '2020-11-22 17:45:14', '0');
INSERT INTO hdw_dubbo.t_sys_notice (ID, TITLE, MSG_ABSTRACT, MSG_CONTENT, START_TIME, END_TIME, PRIORITY, MSG_CATEGORY, MSG_TYPE, SEND_STATUS, SEND_TIME, CANCEL_TIME, USER_IDS, BUS_TYPE, BUS_ID, OPEN_TYPE, OPEN_PAGE, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME, DEL_FLAG) VALUES (6, '测试3', '测试3', '测试3', '2020-10-14 11:20:00', '2020-12-31 23:59:59', 'L', '2', 'ALL', '1', '2020-11-22 17:45:01', null, null, null, null, null, null, 'admin', '2020-09-21 17:22:47', 'admin', '2020-11-22 17:45:01', '0');
INSERT INTO hdw_dubbo.t_sys_notice (ID, TITLE, MSG_ABSTRACT, MSG_CONTENT, START_TIME, END_TIME, PRIORITY, MSG_CATEGORY, MSG_TYPE, SEND_STATUS, SEND_TIME, CANCEL_TIME, USER_IDS, BUS_TYPE, BUS_ID, OPEN_TYPE, OPEN_PAGE, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME, DEL_FLAG) VALUES (7, '测试4', '测试4', '测试4', '2020-10-14 11:30:00', '2020-12-31 23:59:59', 'H', '1', 'ALL', '2', '2020-11-22 17:44:39', '2020-11-22 18:31:24', null, null, null, null, null, 'admin', '2020-10-14 11:28:14', 'admin', '2020-11-22 18:31:24', '0');
