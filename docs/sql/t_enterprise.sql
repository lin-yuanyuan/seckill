create table t_enterprise
(
    id                      bigint auto_increment comment '主键ID'
        primary key,
    business_license_number varchar(255)      null comment '工商注册码',
    enterprise_code         varchar(20)       null comment '企业编号',
    enterprise_name         varchar(255)      null comment '企业名称',
    industry_code           varchar(20)       null comment '所属行业',
    area_code               varchar(20)       null comment '所属区域',
    enterprise_type         tinyint           null comment '企业类型(国企:0，民企:1，私企:2，外企:3)',
    telephone               varchar(60)       null comment '企业联系电话',
    email                   varchar(255)      null comment '企业邮箱',
    zip_code                varchar(255)      null comment '邮政编码',
    legal_person            varchar(60)       null comment '法人',
    main_person             varchar(255)      null comment '企业负责人姓名',
    main_person_mobile      varchar(255)      null comment '企业负责人电话',
    map_x                   varchar(255)      null comment 'x坐标',
    map_y                   varchar(255)      null comment 'y坐标',
    map_z                   varchar(255)      null comment 'z坐标',
    address                 varchar(255)      null comment '地址',
    status                  tinyint default 0 null comment '企业状态（0-正常，1-禁用）',
    create_time             datetime          null comment '记录创建时间',
    update_time             datetime          null comment '记录最后修改时间',
    create_user             varchar(20)       null comment '记录创建者(用户)',
    update_user             varchar(20)       null comment '记录最后修改者(用户)'
)
    comment '企业信息表' charset = utf8
                         row_format = DYNAMIC;

INSERT INTO hdw_dubbo.t_enterprise (id, business_license_number, enterprise_code, enterprise_name, industry_code, area_code, enterprise_type, telephone, email, zip_code, legal_person, main_person, main_person_mobile, map_x, map_y, map_z, address, status, create_time, update_time, create_user, update_user) VALUES (1, '20181218', '96', '测试企业1', '0', '0', 1, '13888888888', 'tuminglong@126.com', '430071', '测试人', '测试人', '13888888888', '100', '100', '100', '测试', 0, '2018-12-18 10:56:46', '2020-04-08 18:46:02', null, 'admin');
