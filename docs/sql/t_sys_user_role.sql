create table t_sys_user_role
(
    id      bigint auto_increment comment '主键id'
        primary key,
    user_id bigint not null comment '用户id',
    role_id bigint not null comment '角色id'
)
    comment '用户角色表' charset = utf8
                         row_format = DYNAMIC;

create index idx_user_role_ids
    on t_sys_user_role (user_id, role_id);

INSERT INTO hdw_dubbo.t_sys_user_role (id, user_id, role_id) VALUES (2541, 1, 1);
INSERT INTO hdw_dubbo.t_sys_user_role (id, user_id, role_id) VALUES (2536, 2, 2);
INSERT INTO hdw_dubbo.t_sys_user_role (id, user_id, role_id) VALUES (2533, 3, 3);
