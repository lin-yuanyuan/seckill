create table t_shop_seckill_product
(
    id            bigint auto_increment comment '秒杀商品id'
        primary key,
    product_id    bigint      null comment '商品id',
    seckill_price decimal     null comment '秒杀价格',
    intergral     bigint      null comment '积分',
    stock_count   int         null comment '库存总数',
    start_date    varchar(32) null comment '秒杀日期',
    time          int         null comment '秒杀场次',
    create_time   varchar(32) null comment '创建时间',
    update_time   varchar(32) null comment '更新时间'
)
    comment '秒杀服务的商品表';

INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (1, 1, 2000, null, 0, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (2, 2, 8000, null, 1000, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (3, 3, 5000, null, 1000, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (4, 4, 2000, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (5, 5, 15000, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (6, 6, 4000, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (7, 7, 300, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (8, 8, 1000, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (9, 9, 3000, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (10, 10, 300, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (11, 11, 500, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (12, 12, 100, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (13, 13, 300, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (14, 14, 6000, null, 600, '2024-05-08', 12, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (15, 1, 2000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (16, 2, 8000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (17, 3, 5000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (18, 4, 2000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (19, 5, 15000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (20, 6, 4000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (21, 7, 300, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (22, 8, 1000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (23, 9, 3000, null, 599, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (24, 10, 300, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (25, 11, 500, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (26, 12, 100, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (27, 13, 300, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (28, 14, 6000, null, 600, '2024-05-08', 14, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (29, 1, 2000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (30, 2, 8000, null, 599, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (31, 3, 5000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (32, 4, 2000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (33, 5, 15000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (34, 6, 4000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (35, 7, 300, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (36, 8, 1000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (37, 9, 3000, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (38, 10, 300, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (39, 11, 500, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (40, 12, 100, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (41, 13, 300, null, 600, '2024-05-08', 16, null, null);
INSERT INTO hdw_dubbo.t_shop_seckill_product (id, product_id, seckill_price, intergral, stock_count, start_date, time, create_time, update_time) VALUES (42, 14, 6000, null, 600, '2024-05-08', 16, null, null);
