create table t_sys_file
(
    id               bigint       not null
        primary key,
    table_id         varchar(50)  null comment '附件类型(哪个表的附件)',
    record_id        varchar(32)  null,
    attachment_group varchar(50)  null comment '表的记录Id下的附件分组的组名',
    attachment_name  varchar(255) null comment '附件名称',
    attachment_path  varchar(255) null comment '附件路径',
    attachment_type  tinyint      null comment '附件类型(0-word,1-excel,2-pdf,3-jpg,png,4-其他)',
    enterprise_id    bigint       null,
    save_type        tinyint      null comment '存储类型（0：本地存储，1:fastdfs）',
    create_time      datetime     null comment '记录创建时间',
    update_time      datetime     null comment '记录最后修改时间',
    create_user      varchar(20)  null comment '记录创建者(用户)',
    update_user      varchar(20)  null comment '记录最后修改者(用户)'
)
    comment '附件表' charset = utf8
                     row_format = DYNAMIC;

create index table_id
    on t_sys_file (table_id, record_id, attachment_group);

INSERT INTO hdw_dubbo.t_sys_file (id, table_id, record_id, attachment_group, attachment_name, attachment_path, attachment_type, enterprise_id, save_type, create_time, update_time, create_user, update_user) VALUES (1247838103305990145, 't_enterprise', '1', '企业', '学习笔记.jpg', 'http://localhost:8182//upload/enterprise/20200408/1586342757782.jpg', 3, null, 0, '2020-04-08 18:46:02', null, 'admin', null);
