create table t_sys_role
(
    id             bigint auto_increment comment '主键id'
        primary key,
    name           varchar(64)       not null comment '角色名',
    seq            tinyint default 0 not null comment '排序号',
    description    varchar(255)      null comment '简介',
    status         tinyint default 0 not null comment '状态(0：开启，1：关闭)',
    create_time    datetime          null on update CURRENT_TIMESTAMP comment '记录创建时间',
    update_time    datetime          null on update CURRENT_TIMESTAMP comment '记录最后修改时间',
    create_user_id bigint            null comment '记录创建者ID'
)
    comment '角色表' charset = utf8
                     row_format = DYNAMIC;

INSERT INTO hdw_dubbo.t_sys_role (id, name, seq, description, status, create_time, update_time, create_user_id) VALUES (1, 'admin', 1, '超级管理员', 0, '2020-11-22 17:44:03', '2020-11-22 17:44:04', 1);
INSERT INTO hdw_dubbo.t_sys_role (id, name, seq, description, status, create_time, update_time, create_user_id) VALUES (2, '应用管理员', 2, '应用管理员', 0, '2020-11-22 17:05:58', '2020-11-22 17:05:59', 1);
INSERT INTO hdw_dubbo.t_sys_role (id, name, seq, description, status, create_time, update_time, create_user_id) VALUES (3, '测试人员', 3, '测试人员', 0, '2020-11-22 17:06:30', '2020-11-22 17:06:31', 1);
INSERT INTO hdw_dubbo.t_sys_role (id, name, seq, description, status, create_time, update_time, create_user_id) VALUES (4, '运维人员', 4, '运维人员', 0, '2020-11-22 17:07:29', '2020-11-22 17:07:29', 1);
INSERT INTO hdw_dubbo.t_sys_role (id, name, seq, description, status, create_time, update_time, create_user_id) VALUES (5, '运营人员', 5, '运营人员', 0, '2020-11-22 17:08:15', '2020-11-22 17:08:16', 1);
