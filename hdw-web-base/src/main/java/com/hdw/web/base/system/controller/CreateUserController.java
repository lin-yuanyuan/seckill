package com.hdw.web.base.system.controller;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import com.alibaba.fastjson.JSON;
import com.hdw.api.base.system.entity.SysUser;
import com.hdw.api.base.system.service.ISysUserService;
import com.hdw.common.core.api.CommonResult;
import com.hdw.common.core.utils.HttpClientUtil;
import com.hdw.common.core.utils.JacksonUtil;
import com.hdw.web.base.shiro.ShiroUtil;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/createUser")
public class CreateUserController {

    private static Logger LOGGER = LoggerFactory.getLogger(CreateUserController.class);
    static String LOGIN_URL = "http://localhost:8385/sys/login";

    @DubboReference
    private ISysUserService ISysUserService;

    @GetMapping("/token")
    public CommonResult<String> createUserLoginToken() throws IOException {

//		List<SysUser> sysUserList = getUserList();
//		System.out.println("添加100个用户到数据库中！！！！");

        //从数据库获取用户
        List<SysUser> sysUserList = userList();

        //用于存储生成的token
        File file = new File("D:/home/hdw_dubbo/token/tokens.txt");
        if (file.exists()) {
            file.delete();
        }
        //创建目录
        file.createNewFile();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));

        for (int i = 0; i < sysUserList.size(); i++) {
            SysUser user = sysUserList.get(i);

            //对密码和用户名进行加密
            AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, "1234567812345678".getBytes(), "1234567812345678".getBytes());
            // 加密
            String name = aes.encryptBase64(user.getName());
            String password = aes.encryptBase64("123456");

            Map<String, String> params = new HashMap<>();
            params.put("username", name);
            params.put("password", password);
            params.put("captcha", "ABCDE");
            String json = JacksonUtil.toJson(params);

            //登陆获取token
            String data = HttpClientUtil.httpPostRequest(LOGIN_URL, json);
            String token = JSON.parseObject(data).getJSONObject("data").getString("token");

            //写入文档
            bw.write(token);
            bw.newLine();

        }
        bw.close();
        System.out.println("*********************************写入完毕*************************************");


        //将获取的token写入文档中

        return CommonResult.success("用户登陆的token获取成功！");
    }

    public List<SysUser> getUserList() {
        List<SysUser> userList = new ArrayList<>();
        //添加登录用户
        for (int i = 101; i < 500; i++) {
            int y = i + 1;
            SysUser user = new SysUser();
            user.setCreateTime(new Date());
            user.setLoginName("lyy" + y);
            user.setName("lyy" + y);
            user.setPassword("123456");
            //对密码进行加密操作
            String salt = ShiroUtil.getRandomSalt(16);
            user.setSalt(salt);
            String pwd = ShiroUtil.md5(user.getPassword(), user.getLoginName() + salt);
            user.setPassword(pwd);

            user.setUserType(1);
            user.setPhone("1912921" + i);
            user.setIsLeader(1);
            userList.add(user);
            ISysUserService.saveByVo(user);
        }
        System.out.println("添加到数据库中的用户信息为：" + userList);
        return userList;
    }

    public List<SysUser> userList() {
        return ISysUserService.list();
    }


}
